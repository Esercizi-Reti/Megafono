const keys = require('./keys/keys');
const express = require('express');
const url = require('url');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser');

var oauthRoutes = require('./routes/oauth-routes');
var rabbitRoutes = require('./routes/rabbit-routes');
var app = express();

const portn = 3000;

app.use(express.static(__dirname + '/public')); //make public css
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));

//cookie control and authenticate
app.use(cookieParser());
app.use(function(req,res,next) {
  var token = req.cookies.auth;
  // decode token
  if (token) {
    jwt.verify(token, keys.cookies.secret, function(err, token_data) {
      if (err) {
         console.log(err.message + ', clearing');
         res.clearCookie('auth');
         next();
      } else {
        req.user_data = token_data;
        // console.log('token found');
        //console.log(token_data);
        next();
      };
    });
    } else {
      console.log('No token found');
      next();
    };
});

app.use('/auth', oauthRoutes);
app.use('/rabbit', rabbitRoutes);

//DB connection
mongoose.connect(keys.mongodb.dbURI, ()=>{
  console.log('Connected to MongoDB...');
});

app.get('/position', (req,res)=>{
  res.render('position');
});

app.get ('/chat', (req,res)=>{
  res.render('messages');
});

app.get('/', (req,res)=>{
  res.render('home', {user : req.user_data});
});


app.listen(portn,()=>{
  console.log('Listening on port' + portn);
});
