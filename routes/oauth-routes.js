var router = require('express').Router();
var googleAuth = require('../Modules/googleAuth');

router.use('/google', googleAuth);

router.get('/profile', (req,res)=>{
  res.render('profile', {user :req.user_data});
});

router.get('/logout', (req,res)=>{
  res.clearCookie('auth');
  res.redirect('/');
});

module.exports = router;
