const getPlaceByCoords = require('../Modules/getPlaceByCoords');
const rabbit = require('../Modules/rabbit');
var router = require('express').Router();



var channel, connection;

var coords = {
  latitude : '',
  longitude : ''
};

var firstGET = true;


rabbit.setConn(function(conn, ch){
  connection = conn;
  channel = ch;
});

router.get('/messages', (req,res)=>{
  // console.log('rabbit-routes.GET/messages: OK');
  rabbit.getMessages(function(result){  // result = messBuff
    console.log('rabbit.routes >>> buffer.length: ' + result.length);
    res.send(JSON.stringify(result));
    res.end();
  });
});


//gestione invio messaggi da parte del client
router.post('/messages', (req,res)=>{
  rabbit.send(channel, coords, req.user_data, req.body.text);
  res.end();
});

//gestione della memorizzazione della posizione
router.post('/position', (req,res)=>{
  console.log('Getting position...');
  console.log(req.body);
  // console.log('POST /position ' + req.body.coords.latitude);  // OK
  if (coords!=req.body.coords){
    // console.log('rabbit.routes /position - I m starting to listen messages');
    coords = req.body.coords;
    var lang = 'IT';
    rabbit.consuming(channel, coords);
    getPlaceByCoords(coords, lang, (result)=>{
          res.send(result[3].formatted_address);
          res.end();
    });
  };

});
module.exports = router;
