var router = require('express').Router();
const https = require('https');
const keys = require('../keys/keys');
const cookieParser = require('cookie-parser');
const User = require('../models/user-models');
const url = require('url');
const jwt = require('jsonwebtoken');
const getPem = require('rsa-pem-from-mod-exp');
const crypto = require('crypto');
// const base64url = require('base64url');
var certs = '';

//settaggio parametri di richiesta del code
var options = {
    hostname: 'accounts.google.com',
    //port: '443',
    method: 'GET',
    path: '/o/oauth2/v2/auth?'+
                      '&scope=openid%20email%20profile'+
                      '&include_granted_scoper=true'+
                      '&state=state_parameter_passthrough_value&'+
                      '&redirect_uri=http://localhost:3000/auth/google/redirect'+
                      '&response_type=code'+
                      '&client_id='+keys.google.id
};

//gestione della prima richiesta /auth/google
router.get('/', (req,res)=>{
  //richiesta certificati google per autenticazione con google public key
  var getCerts = https.request('https://www.googleapis.com/oauth2/v3/certs', (result)=>{
    result.setEncoding('utf8');
    var Tcerts='';
    result.on('data', (chunk)=>{
      Tcerts +=chunk;
    });
    result.on('end', ()=>{
      certs = JSON.parse(Tcerts);
      // console.log(certs.keys[0]);
    });
  });

  getCerts.end();  //chiusura collegamento

  //collegamento con il server e get con i parametri
  var httpReq = https.request(options, (result)=>{
    result.setEncoding('utf8');

    //verifica se google manda un redirezionamento (codice 302)
    if (result.statusCode=='302'){
      console.log('Redirect');
      //richiesta e modifica parametri di redirezionamento
      // options.hostname=url.parse(result.headers.location).hostname;
      // options.path=url.parse(result.headers.location).path;
      // console.log(url.parse(result.headers.location).hostname+url.parse(result.headers.location).path);
      res.redirect('https://'+url.parse(result.headers.location).hostname+url.parse(result.headers.location).path);
      // res.redirect('/auth/google');  //ricarica pagina con nuovi parametri
    }else{

      //ricezione dei pacchetti
      var page='';
      result.on('data',(chunk)=>{
        page+=chunk;
      });

      //pubblicazione della pagina
      result.on('end', ()=>{
        res.send(page);  //stampa della pagina ricevuta
      });
    };
  });
  httpReq.end(); //chiusura della connessione
});

//gestione del codice inviato all'indirizzo /google/redirect
router.get('/redirect',(req,res)=>{

  //scrittura del body
  var data = 'code='+ req.query.code+   //invio del codice appena ricevuto da google
    '&client_id='+ keys.google.id+
    '&client_secret='+ keys.google.secret+
    '&redirect_uri=http://localhost:3000/auth/google/redirect'+
    '&grant_type=authorization_code';

  //scrittura parametri nuovi e metodo per post
  var options2 = {
    hostname: 'www.googleapis.com',
    method: 'POST',
    path: '/oauth2/v4/token',
    headers: {
      'Content-type': 'application/x-www-form-urlencoded',
      'Content-length': Buffer.byteLength(data)
    }
  };

  //Post del codice con parametri
  var httpPost = https.request(options2, (result)=>{
    page='';
    console.log('Status Code: '+result.statusCode);  //codice di controllo
    result.on('data', (chunk)=>{
      page+=chunk;
    });
    result.on('end', ()=>{
      tokenInfo =JSON.parse(page);  //parse del corpo in formato JSON
      // console.log(tokenInfo.id_token);

      //separazione e filtraggio dei dati
      var parts = tokenInfo.id_token.split('.');
      var headerBuf = Buffer.from(parts[0], 'base64');
      var bodyBuf= Buffer.from(parts[1], 'base64');
      var header = JSON.parse(headerBuf.toString());
      var body = JSON.parse(bodyBuf.toString());
      // console.log('Stampa header token');
      // console.log(header);
      // console.log('Stampa body token');
      // console.log(body);


      //Controllo signature con i certificati
      var i;
      for (i=0; i< certs.keys.length; i++){
        if (certs.keys[i].kid==header.kid)
          break;
      };
      // console.log(certs.keys[i]); //certificate matched
      var pem = getPem(certs.keys[i].n, certs.keys[i].e);
      // console.log(pem);
      var verifier = crypto.createVerify('RSA-SHA256');
      verifier.update(parts[0]+'.'+parts[1]);
      if (!verifier.verify(pem, parts[2], 'base64')){
        console.log('ERROR: Signature not valid');
        res.send('ERROR: Signature not valid');
        return -1;
      }
        else
        console.log('Signature valid');

      //preparazione dei dati per il db
      var nick = {
        username : body.name,
        googleId : body.sub,
        thumbnail: body.picture
      };
      //controllo dati nel database
      User.findOne({googleId: nick.googleId}).then((currentUser)=>{
        if (currentUser){
          console.log('User already in db');
          console.log(currentUser);
        }else{
          console.log('Creating new user');
          var newUser = new User(nick);
          //salvataggio  nel database
          newUser.save(function(err){
              if (err) throw err;
              console.log('user created successfully');
          });
        }
      });

      //creazione e salvataggio del cookie
      var token = jwt.sign(nick, keys.cookies.secret, {expiresIn: tokenInfo.expires_in});
      res.cookie('auth', token);

      res.redirect('http://localhost:3000/auth/profile'); //ritorno alla homepage
      return;
    });
  });
  httpPost.write(data);  //post dei dati
});

module.exports=router;
