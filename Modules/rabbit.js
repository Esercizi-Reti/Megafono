var amqp = require('amqplib/callback_api');
var getKeys = require('../modules/getKeys');
var crypto = require('crypto');

var ex = 'megafono';
var emitKeys;


exports.setConn = function(callback){
    amqp.connect('amqp://localhost', function(err_conn, conn) {  // UnixPort: 5672 or null - WinPortNumber: 5673
        if (!err_conn){
            console.log('Rabbit connection: OK');
            conn.createChannel(function(err_ch, ch) {
                if (!err_ch){
                    console.log('Rabbit channel created!');
                    ch.assertExchange(ex, 'direct', {durable:false});
                    callback(conn, ch);
                }
            });
        }
        else
          console.log(err_conn);
    });
}


exports.send = function(ch, coords, user_data, msg){
    var now = Date.now();
    getKeys(coords, function(result){
        emitKeys = result;
        emitKeys.forEach(function(key){
            //preparing data
            var msgBuffer = { username : user_data.username,
                              thumbnail : user_data.thumbnail,
                              text: msg };
            var hashCode = crypto.createHash('md5').update(JSON.stringify(msgBuffer) + now).digest('hex');

            ch.publish(ex, key,new Buffer(JSON.stringify(msgBuffer)), {messageId : hashCode, timestamp : now}); //new Buffer(msg)  // OSS: we used google user id for message id
            //console.log('rabbit.send >>> ' + msg + ' - key : ' + key + ' - MessageId(GoogleUserId) : ' + userId + ' - timestamp : ' + now);
        });

    });
}

exports.connClose = function(conn) {
    conn.close();
}

var messBuff = [];
var controlHash = [];
// merge of setQueue and startConsume functions
exports.consuming = function(ch, coords){
    ch.assertQueue('', {exclusive: true}, function(err, q) {
        getKeys(coords, function(result){
            var listenKeys = result;
            listenKeys.forEach(function(key){
                ch.bindQueue(q.queue, ex, key);
            });
        });

        console.log('rabbit.consuming >>> Queue setting and binding OK. ');

        ch.consume(q.queue, function(msg){
            if(!controlHash.includes(msg.properties.messageId)){
              controlHash.push(msg.properties.messageId);
              var content = JSON.parse(msg.content);
              // console.log('Consume msg.content: '+content.username+ ': '+ content.text);
              messParsed = {
                message : content.text,
                username : content.username,
                thumbnail : content.thumbnail,
                time : msg.properties.timestamp
              };
              messBuff.push(messParsed);
            }
        });

    });
}


exports.getMessages = function(callback){
    //console.log('rabbit.getMessages >>> messBuff: ' + messBuff);
    callback(messBuff);
    messBuff = [];
    controlHash = [];
    //console.log('rabbit.getMessages >>> messBuff empty : ' + messBuff);

}
