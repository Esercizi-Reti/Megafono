//modules/getKeys.js
//generate the messages keys about a coord

//var setCoords = require('./setCoords');


module.exports = function(coords, callback) {

	var lat = setCoord(coords.latitude);
	var lon = setCoord(coords.longitude);

	var emitKeys = [];  // array contenente le chiavi generate

	emitKeys.push((parseInt(lat)-1) + '' + (parseInt(lon)+1));
	emitKeys.push((parseInt(lat)) + '' + (parseInt(lon)+1));
	emitKeys.push((parseInt(lat)+1) + '' + (parseInt(lon)+1));
		
	emitKeys.push((parseInt(lat)-1) + '' + (parseInt(lon)));
	emitKeys.push(lat+lon);
	emitKeys.push((parseInt(lat)+1) + '' + (parseInt(lon)));
		
	emitKeys.push((parseInt(lat)-1) + '' + (parseInt(lon)-1));
	emitKeys.push((parseInt(lat)) + '' + (parseInt(lon)-1));
	emitKeys.push((parseInt(lat)+1) + '' + (parseInt(lon)-1));

	callback(emitKeys);

}


function setCoord(coord){
	var s = coord.toString();
	var result = '';

	for (let i = 0; i < s.length; i++) {
		if (s.charAt(i) == '.'){
			result += s.charAt(i+1);
			result += s.charAt(i+2);
			return result;
			break;
		}
		result += s.charAt(i);
	}
}