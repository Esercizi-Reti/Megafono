var https = require('https');
const keys = require('../keys/keys');

//getting place
module.exports = function(coords,lang, callback){
    options = {
      hostname : 'maps.googleapis.com',
      method : 'GET',
      path : '/maps/api/geocode/json?'+
          'latlng='+ coords.latitude+','+coords.longitude+
          '&language='+'IT'+
          '&key=' + keys.google.apiKey
    }
    var getPlace = https.request(options, (result)=>{
      result.setEncoding('utf8');
      var data = '';
      result.on('data', (chunk)=>{
        data += chunk;
      });
      result.on('end', ()=>{
        data = JSON.parse(data).results;
        // console.log(data);
        callback(data);
      });
    });
    getPlace.end();
  };
