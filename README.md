# Megafono
Megafono è un RESTful web service, la cui funzionalità principale è quella di far comunicare i propri utenti in base alla loro localizzazione. Megafono infatti permette a ciascun utente, previa autenticazione tramite Google, di poter inviare/ricevere messaggi verso/da tutti quegli utenti che si trovano nel raggio di circa 800 metri.

## How does it work
Megafono interfaccia diversi servizi esterni.
Affida l'autenticazione a google, implementando il protocollo OAuth. I dati richiesti a Google vendono memoriazzati e gestiti mediante una base di dati. Una volta geolocalizzato l'utente, stampa la città in cui si trova, mediante un servizio messo a disposizione da Google Maps. La gestione dei messaggi viene affidata ad un broker.

## Tecnologie usate
* [Node.js] + [Express]
* Base di dati (non relazione) - [MongoDB]
* Broker di messaggi - [RabbitMQ]

## Installazioni ed esecuzione
* Assicurarsi che sulla macchina che fungerà da server siano correttamenti installati [Node.js], [MongoDB] e [RabbitMQ]
* Scaricare l'intero progetto dalla pagina [Megafono]
* Fornire le chiavi necessarie, come descirtto in [keys]
* Posizionarsi nella cartella dell'applicazione ed avviarla

```sh
$ cd your/path/Megafono
$ npm install
$ node app
```

* Utilizzare l'applicazione tramite web browser alla pagina

[localhost:3000](localhost:3000)

## API
* __Struttura coordinate__
```javascript
var coords = {
    latitude : '',
    longitude : ''
};
```

* __postPosition__ - per inizializzare lo scambio di messaggi occorre comunicare prima la posizione. Si riceve in risposta la città da cui si effettua la richiesta
```javascript
type: 'POST',
url: '/rabbit/position',
mimeType: 'text/html',
data: {
    "coords" : coords
},
```

* __postMessages__ - si posta il testo del messaggio
```javascript
type: 'POST',
url: '/rabbit/messages',
mimeType: 'text/html',
data: {
    "text" : text
}
```

* __getMessages__ - si riceve in risposta un array con gli eventuali messaggi da leggere
```javascript
type: 'GET',
dataType: 'json',
url: '/rabbit/messages'
```

## Autori
* Castagnino Gianfranco
* Desantis Francesco

## Licenza
???

### Ringraziamenti
Si tiene a ringraziare vivamente il Comune di Sutri, in particolar modo gli addetti alla gestione della [biblioteca comunale], archivio e museo storico della città, per averci permesso di usufruire dei locali, utilizzati come luogo d'incontro per la realizzazione di questo progetto.


[Node.js]:<https://nodejs.org>
[Express]:<https://expressjs.com>
[MongoDB]:<https://www.mongodb.com>
[RabbitMQ]:<https://www.rabbitmq.com>
[keys]:<https://gitlab.com/Esercizi-Reti/Megafono/tree/master/keys>
[Megafono]:<https://gitlab.com/Esercizi-Reti/Megafono>
[biblioteca comunale]:<http://www.comune.sutri.vt.it/il-comune/47-orario-e-contatti-della-biblioteca-comunale/>