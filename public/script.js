var coords = {
    latitude : '',
    longitude : ''
};

var chatready = false;

$(document).ready(function() {
  resetChat();
  //callMessages();
  alert("Waiting for position...");
  getPosition();


  $(".mytext").on("keydown", function(e){
      if (chatready){
        if (e.which == 13){  // enter click
                var text = $(this).val();
                if (text !== ""){
                    postMessages(text);
                    $(this).val('');
                };
            };
            if (e.which == 17){  // ctrl click
                getMessages();
            };
      }

  });

  $('body > div > div > div:nth-child(2) > span').click(function(){
      $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
  });


  // function callMessages(){
  //   //setInterval(function(){$(".mytext").trigger({type: 'keydown', which: 17, keyCode: 17})}, 3000);
  //   setTimeout(function(){
  //       getMessages()
  //   }, 5000);
  // }

});



function getMessages() {
    console.log('GETting messages...');
  $.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rabbit/messages?lat=' + coords.latitude + '&lon=' + coords.longitude,
    //url: '/rabbit/messages',
    success: function(response) {
        if (response){
            //console.log(response.risp);
            if (response== null){ //.message.msg
                console.log('First GET SUCCESS');
            } else {
                console.log('response.msg.content: ' + response);//.message.msg.content.toString());
                response.forEach((res)=>{
                  // console.log(res);
                  insertChat(res);
                });
            }

        };
    }
  });
};


function postMessages(text) {
    console.log('POSTing message...');
    $.ajax({
        type: 'POST',
        url: '/rabbit/messages',
        mimeType: 'text/html',
        data: {
          "text" : text
        }
    });
}


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var strTime = hours + ':' + minutes;
    return strTime;
}


function insertChat(message){
    var control = "";
    var date = formatAMPM(new Date());
    control = '<li style="width:100%">' +
                    '<div class="msj macro">' +
                    '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ message.thumbnail +'" /></div>' +
                        '<div class="text text-l">' +
                            '<h5>'+message.username+'</h5>'+
                            '<p>'+ message.message +'</p>' +
                            '<p><small>'+date+'</small></p>' +
                        '</div>' +
                    '</div>' +
                '</li>';
    $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
};

function resetChat(){
    $("ul").empty();
};

function getPosition(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
      function showPosition(position){
            coords.latitude = position.coords.latitude;
            coords.longitude = position.coords.longitude;
            console.log('GOOGLE COORDS : ' + coords.latitude);
            console.log('GOOGLE COORDS : ' + coords.longitude);
            $.ajax({
                type: 'POST',
                url: '/rabbit/position',
                mimeType: 'text/html',
                data: {
                  "coords" : coords
                },
                success: function(response){
                    chatready = true;
                  $("h5").append("Chat ready! Current position: " + response);
                }
            });
      };
    }else {
      alert("Geolocation is not supported by browser");
    };
  };
