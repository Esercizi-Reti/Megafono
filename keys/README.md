Create a file named "keys.js" with this parameters:

var keys = {
  google: {
    apiKey: '<your Google apiKey>',
    id:'<your Google developer clientID>',
    secret:'<your Google developer clientSecret>'
  },
  mongodb: {
    dbURI:'<your mongodb table path>'
  },
  cookies: {
    secret : '<your cookie secret (es. 'examplecookiesecret0>'
  }
};

module.exports = keys;
